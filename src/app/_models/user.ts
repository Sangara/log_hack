export class User {
    id: number;
    mobile_number: string;
    password: string;
    name: string;
    email: string;
}